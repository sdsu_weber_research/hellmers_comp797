\documentclass[]{article}
%\usepackage{braket}
\usepackage{physics}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage[margin=1.0in]{geometry}

% Title Page
\title{Quantum Computing for Nuclear Physics}
\author{Joe Hellmers}

\begin{document}
\maketitle

\begin{abstract}
I discuss the use of Quantum Computing in the study of nuclear physics.  I will examine the following subjects: 1) quantum algorithms that are expected to be used in devices that have fully error corrected qubits 2) the variational method of quantum mechanics 3) the Variational Quantum Eigensolver (VQE) algorithm for noisy devices 4) a very brief description of how quantum computers are used today in quantum chemistry 5) review some of the literature for current work in the use of quantum computers for nuclear physics 6) a detailed analysis of one method of finding the ground state of a nuclei.
\end{abstract}



\section{Introduction}
The basic ideas for quantum computing have been in development for approximately 40 years, see \cite{FEYNMAN1982} and Appendix A of \cite{YANOFSKY2008}.
For this document I consider gate-based computing platforms, leaving the discussion of quantum annealing computers for another time.  What I mean by gate-based will become apparent in the following sections.

Our main purpose here is to give a quite rudimentary introduction to quantum computing and how it can be applied to the issues of nuclear physics.
It should be considered a work in progress that will be elaborated on as further knowledge is gained by the author.   As this knowledge is increased and extended the author's research capabilities should expand in both the quantum algorithms and nuclear physics involved.

Please note that the acronym QFT in this document will always refer to the quantum Fourier transform not to quantum field theory unless specifically so stated.

\subsection{Qubits}
The Quantum Computing (QC) equivalent of a classical computing bit is the qubit.
A qubit is a logical element that when measured has a state that is either on or off, but while a quantum circuit (to be defined) is executing exists in a superposition of states.
Qubits can be represented as a linear combination of states using the conventional quantum mechanics bra-ket notation.
The states are orthonormal vectors indicating the "off" state and the "on" state


\begin{equation}
\ket{\mqty{ 0 }} = \mqty( 1 \\ 0) \thinspace \& \thinspace \ket{\mqty{ 1 }} = \mqty( 0 \\ 1).
\end{equation}

These basis states can then be combined through a linear combination


\begin{equation}
\rm{Qubit \thinspace State} = \ket{q} = \alpha \ket{\mqty{ 0 }} + \beta \ket{\mqty{ 1 }}. \label{eqn:qubitKetDef}
\end{equation}
Note that in general $\alpha$ and $\beta$ are complex.

When building a quantum circuit the set of qubits used is often called a \textbf{Quantum Register}.

Since $|\alpha|^2$ and $|\beta|^2$ are the probabilities of finding the qubit in state $\ket{0}$ and $\ket{1}$ respectively, they must satisfy the normalization condition

\begin{equation}
|\alpha|^2 + |\beta|^2= 1 \label{eqn:normalizationCondition}.
\end{equation}

For a quantum calculation, a quantum circuit, the qubits used in the calculation are manipulated mathematically by taking the tensor products of their states to form a $2^n$ sized vector, where n is the number of qubits.

For the simple case of a two qubit state it is easy to see

\begin{equation}
\ket{\mqty{ q_0 q_1 }} = \mqty( q_0 \\ q_1) = \mqty( q_{00} \\ q_{01} \\ q_{10} \\ q_{11}).
\end{equation}

In general the state is written as the tensor product of the vectors

\begin{equation}
\ket{\mqty{ u w v }} = \ket u \otimes \ket w \otimes \ket v.
\end{equation}

If the states for each qubit is given as

\begin{equation}
\ket u  = \mqty[u_0 \\ u_1],\ket w = \mqty[w_0 \\ w_1],\ket v = \mqty[v_0 \\ v_1].
\end{equation}

Then the result of the tensor product is

\begin{equation}
\ket{\mqty{ u w v }} = \mqty[u_0w_0 \\ u_0w_1 \\ u_1w_0 \\ u_1w_1]\ket v
=
\mqty[
u_0w_0v_0 \\
u_0w_0v_1 \\
u_0w_1v_0 \\
u_0w_1v_1 \\
u_1w_0v_0 \\
u_1w_0v_1 \\
u_1w_1v_0 \\
u_1w_1v_1 
].
\end{equation}

It is worth noting here again that qubits are quantum mechanical objects and they retain a "mixture" of states only until they are actually measured.
Once a measurement takes place they must collapse to a particular on or off state.

\subsubsection{Qubit Physical Implementations}

Qubits can be implemented by any quantum systems that have two well defined states.
They must also have the capability to be put into an indeterminate state, or a superposition of those two states.  Such a state of the qubit can be mathematically represented by Equation~(\ref{eqn:qubitKetDef}).
Also sets of qubits must be able to become quantum mechanically entangled with other qubits in the system.

Some examples of qubits are superconducting transmons, trapped ions, and quantum dots.
I do not intend to cover hardware to any great extent in this document so I do not include further details here but more information can be found in \cite{TRANSMONPAPER,NAKAHARATEXT}
\subsection{Bloch Sphere}

It is convenient to graphically display the qubit's state with what is called a \textbf{Bloch Sphere}.
For example, if a qubit has $\alpha$ equal to 1 and $\beta$ equal to 0 in Equation~(\ref{eqn:qubitKetDef}), then it is represented graphically on the Bloch Sphere in Figure \ref{fig:blochSphere} by the point on the very top of the sphere.
Similarly when $\alpha$ is 0 and $\beta$ is 1, it is represented by the point on the bottom of the sphere.
If the probabilities of either state are the same then it is a point on the equator of this sphere, where the exact position depends on the phase of the qubit state.

\begin{figure}[ht]
	\centering
	\begin{minipage}{3.0in}
		\includegraphics[width=\linewidth,scale=0.25]{Bloch_sphere}
		\caption{Bloch Sphere qubit representation.\label{fig:blochSphere}}
	\end{minipage}
\end{figure}

\subsection{Quantum Gates}

A qubit can have its state changed by a quantum gate.
A register of qubits is treated mathematically as tensor product of qubits, a vector $\ket{abcd...}$, and quantum gates are matrices that operator on the qubit register's state vector.

An important aspect of all primitive quantum gates is reversibility, which is directly related to erase of information and the unitary nature of the matrix operations that represent them.
Irreversible gates can be constructed by using the CNOT gate, and other reversible gates.
I also note here that a measurement of qubit is an irreversible, non-unitary operation.

\subsubsection{Single Qubit Gates}

In order to transform a qubit from one state to another gate operations are performed.
These operations are typically represented as matrix multiplications.
For example in order to flip the qubit I use a Pauli-X gate to perform the equivalent of the NOT operation on the qubit.

\begin{equation}
\rm X = \mqty[ 0 & 1 \\ 1 & 0]
\end{equation}

Applying the X gate to a qubit in the off state will result in a qubit in the on state.

\begin{equation}
\mqty[ 0 & 1 \\ 1 & 0]\mqty[ 1 \\ 0]=\mqty[ 0  \\ 1]
\end{equation}

There are many single qubit gates \cite{QUANTUMGATES} and I will not list them all here.
However, I will introduce another important single qubit gate, the Hadamard gate $H$. 
The primary purpose of this gate is to put a "pure" state qubit into a superposition of states.

\begin{equation}
H = \frac{1}{\sqrt{2}}\mqty[ 1 & 1 \\ 1 & -1]
\end{equation}

When the Hadamard operates on a qubit in the off state the result is the qubit in a equal mixture state.

\begin{equation}
\frac{1}{\sqrt{2}}\mqty[ 1 & 1 \\ 1 & -1]\mqty[ 1 \\ 0]=\mqty[ 0.7071.. \\ 0.7071..]
\end{equation}


\subsubsection{Two Qubit Gates}

Two qubit gates are operations on two qubits.
Probably the most important two qubit gate is the CNOT gate.
This gate is the principle way that two qubits are entangled in QC.
The logical effect is to act as a conditional NOT gate, that is the value of the second qubit is reversed if the first qubit is on, and is left alone if the first qubit is off.
The first qubit is never modified.

\begin{equation}
\rm CNOT = \mqty[ 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 1 \\0 & 0 & 1 & 0 ]
\end{equation}

So for example if the first qubit is on, and the second qubit is off the result will be a set of qubits where the first qubit state is retained as on and the second is flipped to on.
 
\begin{equation}
\mqty[ 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 1 \\0 & 0 & 1 & 0 ]
\mqty[ 0 \\ 1 \\ 1 \\ 0 ]
=
\mqty[ 0 \\ 1 \\ 0 \\ 1 ]
\end{equation}

Another two qubit gate that is import due to its use in the quantum Fourier transform is the controlled rotation, $CU_1(\theta)$.
This gate rotates a target qubit's $\theta$, see Figure~\ref{fig:blochSphere}, if the control qubit is on.
This is implemented as the operator shown below
\begin{equation}
\rm CU_1(\theta) = \mqty[ 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 \\0 & 0 & 0 & e^{i\theta} ]. \label{eqn:cu1}
\end{equation}

\subsection{Three Qubit Gates}

There are several three qubit gates.
An example is the Toffoli or CCNOT gate.
This gate has the property that if the first two qubits are on then the third qubit is flipped.

\begin{equation}
\rm CCNOT = \mqty[ 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\ 
                   0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\ 
                   0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\ 
                   0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\ 
                   0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\ 
                   0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\ 
                   0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\
                   0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 ]
\end{equation}

For example
\begin{equation}
\mqty[ 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\ 
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 ]
\mqty[ 0 \\ 0 \\ 0 \\ 0 \\ 0 \\ 0 \\ 1 \\ 0]
=
\mqty[ 0 \\ 0 \\ 0 \\ 0 \\ 0 \\ 0 \\ 0 \\ 1]
=
\ket{\mqty{ 1 1 1 }}
\end{equation}

Notice that the last qubit has flipped.

\subsubsection{Quantum Gate Physical Implementations}
While there are a large variety of physical implementations of qubits all quantum gates are implemented by using pulses that are electromagnetic in nature.
For example externally applied magnetic fields, electric fields and laser pulses can be used to implement quantum gates.
The exact wave forms and frequency of these pulses are finely tuned to the precise type of qubits and the gate transformation that is being applied.

\section{High Level Quantum Algorithms}
\label{section:highLevel}
There are several quantum algorithms envisioned that could result in truly ground-breaking calculations that are impossible for classical computers to perform.
The implementation of these require the use of quantum circuits, a few of which I will describe in Section \ref{section:qaPrimitives}.
There are three quantum algorithms that are the "killer apps" for quantum computing.
A "killer app" is an application that is so useful and important to users that it becomes imperative that they have the computing platform on which it can be implemented.
For example, once software to create spreadsheets and documents, such as letters and memos, became available on personal computers, it was imperative for people and businesses to acquire those computers.  So far it appears there are at least three such applications for quantum computers.
The search for additional algorithms is a matter of intense research in government, academia and industry.

All of these algorithms will require quantum computers with the following characteristics.
\begin{itemize}
	\item Large numbers of qubits, $n\thinspace > 10^5$
	\item Long coherence times, that is the time for qubits to maintain their state will need to be significantly longer than the time that it takes to perform gate operations
	\item Error correction, which will increase the number of physical qubits required by one to three orders of magnitude
\end{itemize}
\subsection{Shor's Factorization Algorithm}

Shor's quantum algorithm \cite{SHOR1994} can be used to factor integers in $\log(N)$ polynomial time, where the integer's value is $N$.
The most efficient classical algorithms known for this factorization are in $\log(N)$ exponential time.
Hence it is believed that Shor's factoring algorithm will result in the ability to factor numbers that are much larger than those that can be done now.
The most noteworthy realm of computing that will be affected by this factorization ability is cryptography.
The most commonly used encryption algorithms today rely on the inability of integers to be factored in a reasonable timescale.
The basic process for executing this factoring is to 1) initialize a register with the number of qubits equal to the  integer greater than $2\log_2(N+1)$) 2) perform conditional modulo $2^m$ where $m$ depends on how big the number one is factoring 3) perform a quantum Fourier transform on the qubits which will yield a distribution with multiple peaks 4) measure the qubit register into classical memory 5) analyze the greatest common denominator of the peaks on a classical computer to find the factorization, see chapter 12 in \cite{PROGQCOREILY2019}.
Note that it is computationally fast to check the candidate factors.

The primary quantum circuit primitive used in this algorithm is the \textbf{Quantum Fourier Transform} (QFT).  I discuss the details of this circuit in Section \ref{subsection:QFT}.

\subsection{Grover's Search Algorithm}

Searching through a list of entries is a very common task in computer science.  Typical linear searches through un-indexed data is typically of order $\mathcal{O}(N)$.  Grover's search algorithm \cite{GROVER1997} for quantum computers is order $\mathcal{O}(\sqrt{N})$.  I will not go into much detail for this search algorithm here.  The basic process is to perform a series of phase-logic statements on the qubits required and then perform a conversion of phase differences to amplitudes that can then be read as a distribution when measuring the qubit register.  This is a quantum circuit primitive known as \textbf{Amplitude Amplification} which I will discuss in Section \ref{subsection:AmpAmp}.


\subsection{Simulation of Quantum Multi-body Systems}
\label{subsection:quantumMultiBody}
It would seem to be a truism that quantum computers should be able to simulate quantum systems well.
The difficulties in performing quantum mechanical computations for many-body systems on classical computers of any scale higher than a few particles was probably the main initial impetus for the exploration of quantum information processing \cite{FEYNMAN1982}.
In 1996 Lloyd \cite{LLOYD1996} was able to show how a logic gate based quantum computer could simulate quantum systems much more efficiently than classical computers.

If $n$ components are required to simulate a quantum system, then the number of complex numbers a classical computer needs to process is $c^n$ imaginary numbers where $c$ is some constant that depends on the problem being solved. 
On a quantum computer the same system would require $kn$ qubits, where $k$ is a constant different from $c$.

Some important quantum systems are given below:

\begin{itemize}
	\item Hubbard Model 
	\item Ising Model
	\item Matter-Electromagnetic Interactions
	\item Nuclear Structure Models
\end{itemize}

I will review more details of quantum simulations in section \ref{section:quantumSimulations}

\section{Quantum Algorithm Primitives}
\label{section:qaPrimitives}

The high level algorithms described in Section \ref{section:highLevel} are composed of the primitive quantum circuits I define in this section.
The quantum phase estimation circuit, is itself composed of the primitive inverse QFT circuit, which is for all intents and purposes a special case of QFT.

\subsection{Amplitude Amplification}
\label{subsection:AmpAmp}
Amplitude amplification is a way to detect phase shifts in the qubit register that is being processed.
Phase shifts do not have directly observable effects when performing non-unitary measurements.
This circuit converts phase shifts to readable differences in the resulting qubit register state.

Figure~\ref{fig:AmpAmpCircuit} shows a very simple two qubit version of amplitude amplification.  The first step is to simply put all the qubits into superposition states using the $H$ gate.  Experience indicates that almost all quantum circuits perform operation to being with.  This is the case for all the circuits I discuss here.
 
\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{grover}
		\caption{Two qubit amplitude amplification circuit using Hadamard ($H$), NOT ($X$), and Controlled Z-rotation ($Z$) gates.\label{fig:AmpAmpCircuit}}
	\end{minipage}
\end{figure}

Once the qubit register had the $H$ gate applied I then apply a so called \textbf{oracle}.  This indicates the state I am looking for.  The section of the circuit shown in Figure~\ref{fig:grover00oracle} is the oracle that will find a state of $\ket{00}$.
There are other oracles to find the other states $\ket{01}$, $\ket{10}$, and $\ket{11}$.

\begin{figure}[ht]
	\centering
	\begin{minipage}{3.0in}
		\includegraphics[width=\linewidth,scale=0.25]{grover00oracle}
		\caption{The Oracle used to find the state $\ket{00}$.\label{fig:grover00oracle}}
	\end{minipage}
\end{figure}

When the circuit in Figure~\ref{fig:AmpAmpCircuit} is ran against a real quantum computer the results are shown in Figure~\ref{fig:AmpAmpResults}.
It can be seen that most the most likely state to be found $\ket{00}$.

\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{groverresults}
		\caption{Two qubit amplitude amplifications results.\label{fig:AmpAmpResults}}
	\end{minipage}
\end{figure}

\subsection{Quantum Fourier Transform}
\label{subsection:QFT}

The QFT efficiently finds the the patterns of phases and amplitudes in an incoming quantum register.
In Figure~\ref{fig:qftCircuit} the first step is to put the register into a superposition states.  The next step is to construct a pattern that corresponds to a state of $\ket{001}$.  The rest of the circuit performs the transform and then measures the results.

\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{qft}
		\caption{Three qubit QFT circuit using Hadamard ($H$) and phase shift ($U_1$) gates.\label{fig:qftCircuit}}
	\end{minipage}
\end{figure}

The results from a real quantum computer are shown in Figure~\ref{fig:qftResults}.
As expected the state $\ket{001}$ has the greatest probability.
\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{qftresults}
		\caption{Three qubit QFT results.\label{fig:qftResults}}
	\end{minipage}
\end{figure}
\subsection{Phase Estimation}

Phase estimation provides a way of estimating the eigenvalues of an operator.
An example phase estimation circuit is shown in Figure~\ref{fig:qpeCircuit}.
We see that there are six total qubits.
In this case I use the first four to do the processing and the other two are \textbf{ancilla} qubits uses as scratch space.
The purpose of ancilla qubits is to hold information state of a qubit prior to it being processed in order to potentially use that information later.

\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{qpe}
		\caption{Quantum phase estimation using Hadamard ($H$), NOT ($X$), phase shift ($U_1$), and Controlled-NOT (CNOT) gates. \label{fig:qpeCircuit}}
	\end{minipage}
\end{figure}

Figure~\ref{fig:qpeAncilla} shows the section that stores state information of a qubit in the QPE circuit to ancilla qubit.
This information flows all the way to the end of the circuit and is available for use in an post quantum processing that needs to occur.

\begin{figure}[ht]
	\centering
	\begin{minipage}{3.5in}
		\includegraphics[width=\linewidth,scale=0.25]{qpeancilla}
		\caption{Quantum phase estimation ancillary operations.\label{fig:qpeAncilla}}
	\end{minipage}
\end{figure}

The results for phase estimation circuit shown in Figure \ref{fig:qpeResults}.  One characteristic of these results is that the distribution in the histogram more noise than the in the Amplitude Amplification and QFT results.  This can be be understood by looking at the circuit depths.  The results with less noise of the circuit depths of 8 and the QPE circuit depth of 11.  The greater the depth of a quantum circuit the higher the risk of the signal produced being swamped by errors.

\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{qperesults}
		\caption{Quantum phase estimation results.\label{fig:qpeResults}}
	\end{minipage}
\end{figure}


\section{Quantum Simulations}
\label{section:quantumSimulations}

The essence of all quantum simulations is finding solutions to the Schr\"odinger equation as given in Equation~(\ref{eqn:schrodinger}) where $\hat{H}$ is the Hamiltonian operator.  I have used the convention that $\hbar=1$.

\begin{equation}
i\frac{d}{dt}\ket{\psi}=\hat{H}\ket{\psi} \label{eqn:schrodinger}
\end{equation}

The Hamiltonian is the total energy of the system is written as

\begin{equation}
\hat{H}=\hat{T}+\hat{V}=
\frac{\hat{\textbf{p}}\cdot\hat{\textbf{p}}}{2m}+\hat{V}=
 \frac{1}{2m}(-i\nabla)\cdot(-i\nabla)+\hat{V}=
 -\frac{1}{2m}\nabla^2+\hat{V}.
\label{eqn:hamitonianExtended}
\end{equation}

In order to understand the problems with solving Equations~(\ref{eqn:schrodinger}) and~(\ref{eqn:hamitonianExtended}) I consider a system of $n$ particles which can exist in a linear combination of two different states.  Note that these particles roughly correspond to qubits in a quantum computer.  In order to simulate this system I need to solve $2^n$ differential equations, resulting an in exponential scaling of the problem.

If an initial state can be determined $\ket{\psi(0)}$ then the time evolution of the quantum system is given by

\begin{equation}
\ket{\psi(t)}=e^{-iHt}\ket{\psi(0)}\label{eqn:quantumEvolution}.
\end{equation}

Note that $H$ is usually a large, sparse matrix.

A solution can be approximated by replacing the exponential term in Equation~(\ref{eqn:quantumEvolution}) with a first order polynomial Taylor series for a time step $\Delta t$

\begin{equation}
\ket{\psi(t+\Delta t)}
\approx
(I-iH\Delta t)\ket{\psi(0)},\label{eqn:quantumEvolutionApprox}
\end{equation}

For some cases this approximation may be useful, but often it is not.
In order to make multi-body quantum system simulations more tractable the Hamiltonian is decomposed into a set of Hamiltonians containing only  the strongest interactions of the system.  For example, the Hamiltonians might be only the interactions of the particles closest together, or perhaps only the two body interactions between each particle.

Many systems can have the Hamiltonian can be broken in to a sum of local interactions

\begin{equation}
H(t) = \sum_{i}^{\ell}H_i(t) \label{eqn:hamiltonianDecomp}.
\end{equation}

By using Trotter's formula

\begin{equation}
e^{-iHt} \approx \big(\prod_{i}^{\ell} e^{-iH_it/n} \big)^n, \label{eqn:timeEvolApprox}
\end{equation}

the Hamiltonian time evolution component in Equation~(\ref{eqn:quantumEvolution}) and be approximated. $n$ is the Trotter number and which indicates how accurate the simulation will be \cite{LLOYD1996}.

As an example of this consider the Hamiltonian of a particle in one dimensionw hich can be decomposed into sub-Hamiltonians $H_0$ and $H_1$

\begin{equation}
H=\frac{p^2}{2m}+V(x) \thinspace ; \thinspace H_0=\frac{p^2}{2m};H_1=V(x).
\label{eqn:oneDHamiltonian}
\end{equation}

Typically only a finite region of $x$ is of interest.  I can consider the domain $x\in[-d,d]$ that is divided into subdomains of size $\Delta x$ so that an approximation to the true state $\ket{\psi}$ is given by $\ket{\tilde{\psi}}$ as expressed in 

\begin{equation}
\ket{\tilde{\psi}}=\sum_{k=-d/\Delta x}^{d/\Delta x} a_k \ket{k\Delta x}\label{eqn:psitilde}
\end{equation}

where $a_k$ is the projection of $\ket{\tilde{\psi}}$ on each basis vector $\ket{k\Delta x}$.

The term $e^{-iV(k\Delta x)\Delta t}$ is quite easy to calculate since we know how to calculate $V$ throughout the domain.  The term $e^{-ip^2/2m)\Delta t}$ as seen in Equation~(\ref{eqn:H0Term}) is calculated by using the QFT and fact that the momentum $p$ and position $x$ are conjugate operators.

\begin{equation}
e^{-ip^2/2m)\Delta t}=U_{QFT}e^{-ix^2/2m}U_{QFT}^{\dagger}
\label{eqn:H0Term}
\end{equation}

Note that the total number of qubits required to simulate this system is $n=\log(2d/\Delta x+1)$.

\section{Variational Quantum Eigensolver}

Currently the only quantum computers available have relatively low qubit counts, short coherence times, and low gate fidelity.  This situation is likely to persist for the next few years.  In order to perform quantum computation using what has become known as Noisy Intermediate Scale Quantum (NISQ) computing, specialized algorithms need to be developed.  One of the algorithms that is currently being used and further developed is the Variational Quantum Eigensolver (VQE) \cite{VQE2014}.
The VQE algorithm is primarily used to find the ground state of a system, although it can be modified to find excited states.  This algorithm is related to the standard variational principle found in most quantum mechanics texts.  This method uses a trial wave function to calculate an upper bound on the ground state energy.
In the VQE a very similar concept is used, but in this case it is usually called wave function ansatz, or more often just an ansatz.


The basic process for VQE involves finding the eigenvalues of the decomposed Hamiltonian against an initial state, then classically add the expectation values for the decomposed Hamiltonians which is then optimized to achieve a new state.  This new state is serves as another iteration of into the VQE and the process is continued until the desired accuracy is achieved or the solution fails to converge. 

A conceptual difference between the trail wave function used in introductory quantum mechanics and the ansatz of the VQE, is the the VQE is meant to be parameterized and performing a classical optimization of the ansatz over the parameter space is a key aspect of the algoritm.

\section{Results for the Deuteron}
\label{section:deuteronResults}
The only available calculation done by quantum computing related to nuclear physics as of this writing is that of the binding energy of the deuteron by Dumitrescu et al. \cite{DEUTERON2018}.
A Hamiltonian of the form

\begin{equation}
H_N=\sum_{n,n'=0}^{N-1}\bra{n'}(T+V)\ket{n}a_{n}
a^{\dagger}_{n'}\label{eqn:eftHamiltonian}
\end{equation}
is used where $a^{\dagger}$ and $a$ are creation and annihilation operators respectively.  This Hamiltonian is based on pion-less Effective Field Theory (EFT) for a basis with dimension of $N$.  

Thus far in all the research materials I have uncovered regarding the use of quantum computing for nuclear physics, the approaches that are used are base on second quantization methods, or quantum field theory.

The creation and annihilation operators are taken to be the harmonic-oscillator $s$-wave state $\ket{n}$.
The matrix elements for $T$ and $V$ are given by

\begin{equation}
\bra{n'}T\ket{n}=\frac{\hbar\omega}{2}\big[(2n+3/2)\delta^{n'}_n-\sqrt{n(n+1/2)}\delta^{n'+1}_n-\sqrt{(n+1)(n+3/2)}\delta^{n'-1}_n\big],
\label{eqn:Telements}
\end{equation}

\begin{equation}
\bra{n'}V\ket{n}=V_0\delta^0_n\delta^{n'}_n.
\label{eqn:Velements}
\end{equation}

Note that $T$ is a tridiagonal matrix and $V$ is a matrix with only the element (0,0) equal to a given value of $V_0$.

The creation and annihilation operators are computed from the Jordan-Wigner transformation \cite{JORDAN1928}  
	
\begin{equation}
a^{\dagger}_n \rightarrow \frac{1}{2}\big[ \prod^{n-1}_{j=0}-Z_j\big](X_n-iY_n),
\label{eqn:createOperator}
\end{equation}

\begin{equation}
a_n \rightarrow \frac{1}{2}\big[ \prod^{n-1}_{j=0}-Z_j\big](X_n+iY_n).
\label{eqn:annihOperator}
\end{equation}
	
In these equations $X$, $Y$, and $Z$ are the Pauli operators.

There are three components of the Hamiltonian; $H_1$, $H_2$, and $H_3$.
$H_1$ is just a straightforward calculation of the exact value.
For $H_2$ and $H_3$ we need two different ansatz functions

\begin{equation}
U(\theta)\equiv e^{\theta(a^\dagger_0a_1-a^\dagger_1a_0)}=e^{i(\theta/2)(X_0Y_1-X_1Y_0)},
\label{eqn:ansatzH2}
\end{equation}

\begin{equation}
U(\eta,\theta)\equiv e^{\eta(a^\dagger_0a_1-a^\dagger_1a_0)+\theta(a^\dagger_0a_2-a^\dagger_2a_0)}\approx e^{i(\eta/2)(X_0Y_1-X_1Y_0)}e^{i(\theta/2)(X_0Z_1Y_2-X_2Z_1Y_0)}.
\label{eqn:ansatzH3}
\end{equation}

These are the Unitary Couple Cluster (UCC) ans\"atze.



In order to implement these two and three qubit ans\"atze the circuits shown in Figures~\ref{fig:H2circuit} and~\ref{fig:H3circuit} were implemented on both IBM and Rigetti hardware.  We have excluded the readout operations as well as the superposition operations in these figures.


\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{h2}
		\caption{Two qubit ansatz for the $H_2$ Hamiltonian for the deuteron. Each line represents the state of the 0 and 1 order qubits respectively.\label{fig:H2circuit}}
	\end{minipage}
\end{figure}

\begin{figure}[ht]
	\centering
	\begin{minipage}{4.5in}
		\includegraphics[width=\linewidth,scale=0.25]{h3}
		\caption{Three qubit ansatz for $H_3$ Hamiltonian for the deuteron.\label{fig:H3circuit}}
	\end{minipage}
\end{figure}

Error for the resulting ground state energy calculated for both the IBM and Rigetti computers is less than 4\% compared to the known value.
 
\section{Conclusion and Next Steps}

For the discussion in Section~\ref{section:deuteronResults} I have the following open issues.

\begin{description}
	\item[$\bullet$] What is the "harmonic-oscillator $s$-wave state"?
	\item[$\bullet$] How is $\hbar \omega$ calculated to be -5.686581 MeV?
	\item[$\bullet$] Calculate the asatz for higher numbers of qubits.
	\item[$\bullet$] I would like to find out what might be good Hamiltonians to use for tritium or helium.  Can pionless EFT be used for those nuclei?
	Try to model two and three quark systems?  What might the Hamiltonian be for those?  How many qubits would we need to use?
\end{description}

\bibliographystyle{ieeetr}
\bibliography{bibquantumcomputing}

\end{document}          

